<?php

/**
 * Phalcon MongoDB auto increment
 * 
 * @usage $offer->unique_auto_increment_value = Auto::increment('offer');
 * @author Frank Broersen <frank@pitgroup.nl>
 */
use \Phalcon\Mvc\Collection as Collection;

class Auto extends Collection 
{
    /**
     * The collection for which to count auto increments
     * @var string
     */
    public $collection;
    
    /**
     * The value of the auto increment
     * @var int
     */
    public $value;
        
    /**
     * Increment the value for the given collection
     * @param string $collection
     * @return int the auto incremented value
     */
    public static function increment($collection)
    {
        $counter = Counter::findFirst(array(array(
            'collection' => $collection
        )));
        
        if ( ! $counter) {
            $counter = new Counter();
            $counter->collection = $collection;
            $counter->value = 1;
        } else {
            $counter->value += 1;
        }
        $counter->save();
        
        return $counter->value;        
    }
    
    /**
     * Set collection name to _auto_increment
     */
    public function getSource()
    {
       return '_auto_increment'; 
    }
            
}